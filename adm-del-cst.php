<?php
    
    require 'functions.php';

    $id = $_GET["id"];

    if (hapus_cst_kurir($id) > 0){
        echo "
        <script>
            alert('Data berhasil dihapus');
            document.location.href = 'adm-data-cst.php';
        </script>
        ";
    } else{
        echo "
        <script>
            alert('Data gagal dihapus');
            document.location.href = 'adm-data-cst.php';
        </script>
        ";
    }
        
?>