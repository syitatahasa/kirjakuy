<?php

require 'functions.php';

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

if (isset($_POST["submit"])) {
    if (tambah($_POST) > 0) {
        echo "
    <script>
        alert('Data BERHASIL ditambahkan');
    </script>
    ";
    } else {
        echo "
    <script>
        alert('Data GAGAL ditambahkan');
    </script>
    ";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style-cst.css">

    <title>Dashboard Customer</title>
</head>

<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="profile-user-box card-box bg-primary">
                        <div class="row">
                            <div class="col-sm-6"><span class="float-left mr-3"><img src="img/express-delivery.png" alt="" class="thumb-lg rounded-circle"></span>
                                <div class="media-body text-white">
                                    <br>
                                    <h4 class="mt-1 mb-1 font-18">Selamat Datang di Halaman Customer</h4>
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            <?php echo $_SESSION['username']; ?>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                                        </ul>
                                    </div>
                                    <!-- <p><?php echo $_SESSION['username']; ?></p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="cst.php">Cek Tarif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="cst-transaksi.php">Order</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cst-data-transaksi.php">Riwayat Transaksi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cst-tracking.php">Tracking</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <!-- <div class="col-xl-8"> -->
                    <div class="card-box ">
                        <h4 class="header-title mt-0 mb-3">Order Transaksi</h4>
                        <div class="user">
                            <form action="" method="post" class="user ">
                                <h5 class="text-custom">Pengirim</h5>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="nama_pengirim" placeholder="Nama Lengkap">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="alamat_pengirim" placeholder="Alamat">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="cp_pengirim" placeholder="No Handphone">
                                </div>
                                <hr>
                                <h5 class="text-custom">Tujuan</h5>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="nama_tujuan" placeholder="Nama Lengkap">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="alamat_tujuan" placeholder="Alamat">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="kota_tujuan" placeholder="Kota Tujuan">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="cp_tujuan" placeholder="No Handphone">
                                </div>
                                <hr>
                                <h5 class="text-custom">Paket</h5>
                                <div class="form-group">
                                    <input type="timestamp" class="form-control form-control-user" name="tgl_transaksi" value="<?php date_default_timezone_set("Asia/Jakarta") . $tgl_transaksi = date('d-m-Y H:i');
                                                                                                                                echo $tgl_transaksi; ?>">
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <label class="input-group-text">Jenis Paket</label>
                                        <select class="form-select form-control" name="jenis_paket">
                                            <option selected>Choose...</option>
                                            <option value="Reguler">Reguler</option>
                                            <option value="Kilat">Kilat</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="nama_barang" placeholder="Nama Barang">
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <label class="input-group-text">Jenis Barang</label>
                                        <select class="form-select form-control" name="jenis_barang">
                                            <option selected>Choose...</option>
                                            <option value="Dokumen">Dokumen</option>
                                            <option value="Fashion">Fashion</option>
                                            <option value="Aksesoris">Aksesoris</option>
                                            <option value="Elektronik">Elektronik</option>
                                            <option value="Bahan Pokok">Bahan Pokok</option>
                                            <option value="Lainnya">Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control-user" placeholder="Berat" name="berat">
                                        <span class="input-group-text">Gram</span>
                                    </div>
                                </div>
                                <div class="form-group d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                <!-- </div> -->
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- container -->
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script type="text/javascript"></script>
</body>

</html>