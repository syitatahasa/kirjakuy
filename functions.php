<?php
$conn = mysqli_connect('bariskode.online', 'bariskod', 'passforlife9', 'bariskod_kirjakuy');

function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

function registrasi($data){
    global $conn;
    $nama = addslashes($data["nama"]);
    $username = strtolower($data["username"]);
    $alamat = $data["alamat"];
    $cp = $data["cp"];
    $email = $data["email"];
    $password = mysqli_escape_string($conn, $data["password"]);
    $password2 = mysqli_escape_string($conn, $data["password2"]);
    $role = $data["role"];

    //cek username sudah ada atau belom
    $result = mysqli_query($conn, "SELECT username FROM akun WHERE username = '$username'");

    if (mysqli_fetch_assoc($result)) {
        echo "
            <script>
                alert('username sudah terdaftar');
            </script>
        ";

        return false;
    }

    //cek konfirmasi password
    if ($password != $password2){
        echo "
            <script>
                alert('konfirmasi password tidak sama')
            </script>
        ";
        return false;
        
    } else {
        //enkripsi password
        $password = password_hash($password, PASSWORD_DEFAULT);

        // tambahkan user baru ke database
        mysqli_query($conn, "INSERT INTO akun VALUES ('', '$nama', '$username', '$alamat','$cp', '$email', '$password', 'customer')");
        
        return mysqli_affected_rows($conn);
    }
}
function akun($data){
    global $conn;
    $nama = addslashes($data["nama"]);
    $username = strtolower($data["username"]);
    $alamat = $data["alamat"];
    $cp = $data["cp"];
    $email = $data["email"];
    $password = mysqli_escape_string($conn, $data["password"]);
    $password2 = mysqli_escape_string($conn, $data["password2"]);
    $role = $data["role"];

    //cek username sudah ada atau belom
    $result = mysqli_query($conn, "SELECT username FROM akun WHERE username = '$username'");

    if (mysqli_fetch_assoc($result)) {
        echo "
            <script>
                alert('username sudah terdaftar');
            </script>
        ";

        return false;
    }

    //cek konfirmasi password
    if ($password != $password2){
        echo "
            <script>
                alert('konfirmasi password tidak sama')
            </script>
        ";
        return false;
        
    } else {
        //enkripsi password
        $password = password_hash($password, PASSWORD_DEFAULT);

        // tambahkan user baru ke database
        mysqli_query($conn, "INSERT INTO akun VALUES ('', '$nama', '$username', '$alamat','$cp', '$email', '$password', '$role')");
        
        return mysqli_affected_rows($conn);
    }
}

function tambah($data)
{
    global $conn;
    $tgl_transaksi = $data["tgl_transaksi"];
    $jenis_paket = $data["jenis_paket"];
    $nama_pengirim = addslashes($data["nama_pengirim"]);
    $alamat_pengirim = $data["alamat_pengirim"];
    $cp_pengirim = $data["cp_pengirim"];
    $nama_tujuan = addslashes($data["nama_tujuan"]);
    $alamat_tujuan = $data["alamat_tujuan"];
    $kota_tujuan = $data["kota_tujuan"];
    $cp_tujuan = $data["cp_tujuan"];
    $nama_barang = $data["nama_barang"];
    $jenis_barang = $data["jenis_barang"];
    $berat = $data["berat"];

    $query = "INSERT INTO transaksi VALUES
    ('', '$tgl_transaksi', '$jenis_paket', '$nama_pengirim', '$alamat_pengirim', '$cp_pengirim', '$nama_tujuan', '$alamat_tujuan', '$kota_tujuan', '$cp_tujuan', '$nama_barang', '$jenis_barang', '$berat', '', '', '')
    ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function hapus($id){
    global $conn;
    $query = "DELETE FROM transaksi WHERE id=$id";
    mysqli_query($conn,$query);
    return mysqli_affected_rows($conn);
}

function hapus_cst_kurir($id){
    global $conn;
    $query = "DELETE FROM akun WHERE id=$id";
    mysqli_query($conn,$query);
    return mysqli_affected_rows($conn);
}

function ubah($data){
    global $conn;
    $id = $data["id"];
    $jenis_paket = $data["jenis_paket"];
    $nama_pengirim = addslashes($data["nama_pengirim"]);
    $alamat_pengirim = $data["alamat_pengirim"];
    $cp_pengirim = $data["cp_pengirim"];
    $nama_tujuan = addslashes($data["nama_tujuan"]);
    $alamat_tujuan = $data["alamat_tujuan"];
    $kota_tujuan = $data["kota_tujuan"];
    $cp_tujuan = $data["cp_tujuan"];
    $nama_barang = addslashes($data["nama_barang"]);
    $jenis_barang = $data["jenis_barang"];
    $berat = $data["berat"];
    $stat = $data["stat"];
    $tgl_sampai = $data["tgl_sampai"];
    $nama_penerima = addslashes($data["nama_penerima"]);
    
    $query = "UPDATE transaksi SET
        jenis_paket = '$jenis_paket',
        nama_pengirim = '$nama_pengirim',
        alamat_pengirim = '$alamat_pengirim',
        cp_pengirim = '$cp_pengirim',
        nama_tujuan = '$nama_tujuan',
        alamat_tujuan = '$alamat_tujuan',
        kota_tujuan = '$kota_tujuan',
        cp_tujuan = '$cp_tujuan',
        nama_barang = '$nama_barang',
        jenis_barang = '$jenis_barang',
        berat = '$berat',
        stat = '$stat',
        tgl_sampai = '$tgl_sampai',
        nama_penerima = '$nama_penerima'
        WHERE id= '$id'
    ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function ubah_data_cst_kurir($data) {
    global $conn;
    $id = $data["id"];
    $nama = addslashes($data["nama"]);
    $username = strtolower($data["username"]);
    $alamat = $data["alamat"];
    $cp = $data["cp"];
    $email = $data["email"];

    $query = "UPDATE akun SET
        nama = '$nama',
        username = '$username',
        alamat = '$alamat',
        cp = '$cp',
        email = '$email'
        WHERE id= '$id'
    ";

    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);

}

// function tracking($data) {
//     global $conn;
//     $id = $data["P4K3T"."id"];

//     $result = mysqli_query($conn, "SELECT id FROM transaksi WHERE id = '$id'");

//     if (mysqli_fetch_assoc($result)) {
        
//         header("Location: cst-status.php");

//         return false;
//     }

// }

// function tambah_transaksi($data) {
//     global $conn;
//     $nama_pengirim = $data["nama_pengirim"];
//     $alamat_pengirim = $data["alamat_pengirim"];
//     $cp_pengirim = $data["cp_pengirim"];
//     $nama_tujuan = $data["nama_tujuan"];
//     $alamat_tujuan = $data["alamat_tujuan"];
//     $kota_tujuan = $data["kota_tujuan"];
//     $cp_tujuan = $data["cp_tujuan"];
//     $tgl_transaksi = $data["tgl_transaksi"];
//     $jenis_paket = $data["jenis_paket"];
//     $nama_brg = $data["nama_brg"];
//     $jenis_brg = $data["jenis_brg"];
//     $berat = $data["berat"];

//     $query = "INSERT INTO pengirim VALUES
//     ('', '$nama_pengirim', '$alamat_pengirim', '$cp_pengirim')
//     ";
//     $query .= "INSERT INTO tujuan VALUES
//     ('', '$nama_tujuan', '$alamat_tujuan', '$kota_tujuan', '$cp_tujuan')
//     ";
//     $query .= "INSERT INTO barang VALUES
//     ('', '$nama_brg', '$jenis_brg', '$berat')
//     ";
//     $query .= "INSERT INTO transaksi2 VALUES
//     ('', '$tgl_transaksi', '$jenis_paket', '', '', '', '')
//     ";

//     if ($conn->multi_query($query) === TRUE) {
//         echo "New records created successfully";
//     } else {
//         echo "Error: " . $query . "<br>" . $conn->error;
//     }
    
//     $conn->close();

//     // mysqli_query($conn, $query);
//     // return mysqli_affected_rows($conn);
// }