<?php

require 'functions.php';

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

$id = $_GET["id"];
$rows = query("SELECT * FROM transaksi WHERE id = $id");


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style-cst.css">

    <title>Dashboard Customer</title>
</head>

<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="profile-user-box card-box bg-primary">
                        <div class="row">
                            <div class="col-sm-6"><span class="float-left mr-3"><img src="img/express-delivery.png" alt="" class="thumb-lg rounded-circle"></span>
                                <div class="media-body text-white">
                                    <br>
                                    <h4 class="mt-1 mb-1 font-18">Selamat Datang di Halaman Customer</h4>
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            <?php echo $_SESSION['username']; ?>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                                        </ul>
                                    </div>
                                    <!-- <p><?php echo $_SESSION['username']; ?></p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="cst.php">Cek Tarif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cst-transaksi.php">Order</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="cst-data-transaksi.php">Riwayat Transaksi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cst-tracking.php">Tracking</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="card-box ">
                    <h5 class="header-title mt-0 mb-3">Detail Transaksi</h5> <br>
                    <div class="user">
                        <?php foreach ($rows as $row) : ?>
                            <div class="row align-items-start">
                                <div class="col">
                                    <div class="sidebar-heading">
                                        Data Paket
                                    </div>
                                    <hr class="sidebar-divider">
                                    <dl class="row">
                                        <dt class="col-sm-6">No Pengiriman</dt>
                                        <dd class="col-sm-6"><?php echo "P4K3T", $row["id"] ?></dd>

                                        <dt class="col-sm-6">Tanggal Transaksi</dt>
                                        <dd class="col-sm-6"><?php echo $row["tgl_transaksi"] ?></dd>

                                        <dt class="col-sm-6">Jenis Paket</dt>
                                        <dd class="col-sm-6"><?php echo $row["jenis_paket"] ?></dd>

                                        <dt class="col-sm-6">Nama Barang</dt>
                                        <dd class="col-sm-6"><?php echo $row["nama_barang"] ?></dd>

                                        <dt class="col-sm-6">Jenis Barang</dt>
                                        <dd class="col-sm-6"><?php echo $row["jenis_barang"] ?></dd>

                                        <dt class="col-sm-6">Berat</dt>
                                        <dd class="col-sm-6"><?php echo $row["berat"] ?></dd>
                                    </dl>
                                </div>
                                <div class="col">
                                    <div class="sidebar-heading">
                                        Pengirim
                                    </div>
                                    <hr class="sidebar-divider">
                                    <dl class="row">
                                        <dt class="col-sm-3">Nama</dt>
                                        <dd class="col-sm-9"><?php echo $row["nama_pengirim"] ?></dd>

                                        <dt class="col-sm-3">Alamat</dt>
                                        <dd class="col-sm-9"><?php echo $row["alamat_pengirim"] ?></dd>

                                        <dt class="col-sm-3">Telepon</dt>
                                        <dd class="col-sm-9"><?php echo $row["cp_pengirim"] ?></dd>
                                    </dl>
                                </div>
                                <div class="col">
                                    <div class="sidebar-heading">
                                        Tujuan
                                    </div>
                                    <hr class="sidebar-divider">
                                    <dl class="row">
                                        <dt class="col-sm-3">Nama</dt>
                                        <dd class="col-sm-9"><?php echo $row["nama_tujuan"] ?></dd>

                                        <dt class="col-sm-3">Alamat</dt>
                                        <dd class="col-sm-9"><?php echo $row["alamat_tujuan"] ?></dd>

                                        <dt class="col-sm-3">Kab/Kota</dt>
                                        <dd class="col-sm-9"><?php echo $row["kota_tujuan"] ?></dd>

                                        <dt class="col-sm-3">Telepon</dt>
                                        <dd class="col-sm-9"><?php echo $row["cp_tujuan"] ?></dd>
                                    </dl>
                                </div>
                                <div class="form-group d-grid gap-2 d-md-flex justify-content-md-end">
                                    <a class="btn btn-primary" href="cst-data-transaksi.php" role="button">Back</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container -->
    </div>



        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
        <script type="text/javascript"></script>
</body>

</html>