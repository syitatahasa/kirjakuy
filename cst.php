<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style-cst.css">

    <title>Dashboard Customer</title>
</head>

<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="profile-user-box card-box bg-primary">
                        <div class="row">
                            <div class="col-sm-6"><span class="float-left mr-3"><img src="img/express-delivery.png" alt="" class="thumb-lg rounded-circle"></span>
                                <div class="media-body text-white">
                                    <br>
                                    <h4 class="mt-1 mb-1 font-18">Selamat Datang di Halaman Customer</h4>
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            <?php echo $_SESSION['username']; ?>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                                        </ul>
                                    </div>
                                    <!-- <p><?php echo $_SESSION['username']; ?></p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Cek Tarif</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cst-transaksi.php">Order</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cst-data-transaksi.php">Riwayat Transaksi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cst-tracking.php">Tracking</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4">
                    <div class="card-box">
                        <h4 class="header-title mt-0">Personal Information</h4>
                        <div class="panel-body">
                            <p class="text-muted font-13">Hye, I’m Johnathan Doe residing in this beautiful world. I
                                create websites and mobile apps with great UX and UI design. I have done work with big
                                companies like Nokia, Google and Yahoo. Meet me or Contact me for any queries. One Extra
                                line for filling space. Fill as many you want.</p>
                            <hr>
                            <div class="text-left">
                                <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15"><?php echo $_SESSION['nama']; ?></span></p>
                                <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15"><?php echo $_SESSION['cp']; ?></span></p>
                                <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $_SESSION['email']; ?></span></p>
                            </div>
                            <ul class="social-links list-inline mt-4 mb-0">
                                <li class="list-inline-item"><a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="card-box ">
                        <h4 class="header-title mt-0 mb-3">Cek Ongkos Kirim</h4>
                        <div class="user">
                            <form action="" method="post" class="user ">
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="kota_asal" id="kota_asal" placeholder="Kota Asal *" value="" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="kota_tujuan" id="kota_tujuan" placeholder="Kota Tujuan *" value="" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="jenis_paket" id="jenis_paket" placeholder="Jenis Pengiriman *" value="" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" name="berat" id="berat" placeholder="Berat *" value="" required>
                                </div>
                                <button class="btn btn-primary btn-user btn-block" type="submit" name="cek">Cek Ongkir</button>
                            </form>
                        </div>
                    </div>
                    <!-- <div class="card-box">
                        <h4 class="header-title mb-3">My Projects</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project Name</th>
                                        <th>Start Date</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th>Assign</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Adminox Admin</td>
                                        <td>01/01/2015</td>
                                        <td>07/05/2015</td>
                                        <td><span class="label label-info">Work in Progress</span></td>
                                        <td>Coderthemes</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Adminox Frontend</td>
                                        <td>01/01/2015</td>
                                        <td>07/05/2015</td>
                                        <td><span class="label label-success">Pending</span></td>
                                        <td>Coderthemes</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Adminox Admin</td>
                                        <td>01/01/2015</td>
                                        <td>07/05/2015</td>
                                        <td><span class="label label-pink">Done</span></td>
                                        <td>Coderthemes</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Adminox Frontend</td>
                                        <td>01/01/2015</td>
                                        <td>07/05/2015</td>
                                        <td><span class="label label-purple">Work in Progress</span></td>
                                        <td>Coderthemes</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Adminox Admin</td>
                                        <td>01/01/2015</td>
                                        <td>07/05/2015</td>
                                        <td><span class="label label-warning">Coming soon</span></td>
                                        <td>Coderthemes</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- container -->
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script type="text/javascript"></script>
</body>

</html>